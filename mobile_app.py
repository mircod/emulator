import requests
import json


class MobileApp:
    def __init__(self):
        self.server_host = "http://3.67.201.254/api/v1/"
        self.save_measurement_url = "device/{device_pk}/temp/"
        self.headers = {"Authorization": "Token bacea79adab2baa799ceef9add875f454e60a62c",
                        "content-type": "application/json"}

    def send_measurement_to_server(self, measurement):
        json_data = json.dumps(measurement._asdict())
        response = requests.post(
            url=self.server_host + self.save_measurement_url.format(device_pk=measurement.device),
            data=json_data, headers=self.headers)
        print(response.status_code)
