import time

from device_emulator import Device
from mobile_app import MobileApp

mobile_app = MobileApp()
device = Device(mobile_app=mobile_app)
while True:
    device.send_measurement_to_mobile_app()
    time.sleep(30)
