from typing import NamedTuple
from datetime import datetime
import random

from mobile_app import MobileApp


class Measurement(NamedTuple):
    value: float
    date: str
    device: int


class Device:
    def __init__(self, mobile_app: MobileApp):
        self.ids = [x for x in range(6, 13)]
        self.mobile_app = mobile_app

    def send_measurement_to_mobile_app(self):
        measurement = Measurement(value=round(random.uniform(36, 38), 1), date=str(datetime.now()), device=random.choice(self.ids))
        self.mobile_app.send_measurement_to_server(measurement=measurement)
